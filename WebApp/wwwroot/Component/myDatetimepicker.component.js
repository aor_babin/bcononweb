﻿(function () {
    var module = angular.module('myDatetimepicker',[]);

    function maincontroller($http) {
        var model = this;

        model.$onInit = function () {
            if (model.date != null) {
                model.onSelectChange({ "date": model.date });
            }
        }
    }


    module.component("myDatetimepicker", {
        templateUrl: "/Component/myDatetimepicker.component.html",
        bindings: {
            date: "=",
            onSelectChange:"&"
        },
        controllerAs: "model",
        controller: ["$http", maincontroller]
    });
})();