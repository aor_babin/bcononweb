﻿(function () {
    var module = angular.module('componentTest', []);

    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default:
                return false;
        }
    }

    function maincontroller($http) {
        var model = this;
        model.selectDisplay = null;
        model.selectItem = {};


        model.$onInit = function () {

        };

        model.$onChanges = function () {
            if (!empty(model.selectValue)) {
                for (var i in model.list) {
                    if (model.list[i][model.value] == model.selectValue) {
                        model.selectItem = model.list[i];
                        model.selectDisplay = model.selectItem[model.display];
                        return;
                    }

                }
            }

        };


        model.dropdownSelectChange = function (x) {

            model.selectItem = x;
            model.selectValue = x[model.value];
            model.selectDisplay = x[model.display];

            return ({ "selectValue": model.selectValue });
        };


    }


    module.component("componentTest", {
        templateUrl: "/Component/componenttest.component.html",
        bindings: {
            list: "<",
            display: "<",
            value: "<",
            selectValue: "<",
            placeHolder: "<",
            onSelectChange:"&"

        },
        controllerAs: "model",
        controller: ["$http", maincontroller]
    });
})();