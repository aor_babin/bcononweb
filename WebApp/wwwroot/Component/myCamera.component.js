﻿(function () {

    var module = angular.module("myCamera",[]);

    function maincontroller($http) {
        var model = this;
        model.imgcontent = '';

        model.$onInit = function () {
         
        };

        model.getData = function () {
            model.imgcontent = angular.element('#ImgText').val();
            return { "Photo": model.imgcontent };
        };
    }

    module.component("myCamera", {
        templateUrl: "/Component/myCamera.component.html",
        controllerAs: "model",
        bindings: {
            saveOnclick: "&"
        },
        controller: ["$http", maincontroller]
    });


})();