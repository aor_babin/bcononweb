﻿(function () {
    var module = angular.module('myDropdownList',[]);

    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default:
                return false;
        }
    }
   
    
    function maincontroller($http) {
        var model = this;
        model.selectDisplay = null;
        model.selectItem = {};


        model.$onInit = function () {
            //console.log(model.list.length);
        };

        model.$postLink = function () {
            
            if (model.selectValue == 0) {
                model.selectValue = 0;
                model.selectDisplay = '';

                if (!empty(model.defaultDisplay)) {
                    for (var i in model.list) {
                        if (model.list[i][model.display] == model.defaultDisplay) {
                            model.selectItem = model.list[i];
                            model.selectDisplay = model.selectItem[model.display];
                            model.selectValue = mode.selectItem[model.value];
                            model.onSelectChange({ "selectValue": model.selectValue });
                            console.log(model.selectDisplay);
                            return;
                        }

                    }
                }

            }
            
        };

        model.$onChanges = function () {
          
            //console.log(model.list.length);
            if (!empty(model.selectValue)) {
                for (var i in model.list) {
                    if (model.list[i][model.value] == model.selectValue) {
                        model.selectItem = model.list[i];
                        model.selectDisplay = model.selectItem[model.display];
                        console.log(model.selectDisplay);
                        return;
                    }

                }
            }
            else if (model.selectValue == 0) {


                model.selectValue = 0;
                model.selectDisplay = '';

                if (!empty(model.defaultDisplay)) {
                    for (var i in model.list) {
                        if (model.list[i][model.display] == model.defaultDisplay) {
                            model.selectItem = model.list[i];
                            model.selectDisplay = model.selectItem[model.display];
                            model.selectValue = mode.selectItem[model.value];
                            model.onSelectChange({ "selectValue": model.selectValue, "selectDisplay": model.selectDisplay });
                            return;
                        }

                    }
                }

            }
        };


        model.dropdownSelectChange = function (x) {

            model.selectItem = x;
            model.selectValue = x[model.value];
            model.selectDisplay = x[model.display];

            return ({ "selectValue": model.selectValue, "selectDisplay": model.selectDisplay });
        };

       

    }


    module.component("myDropdownlist", {
        templateUrl: "/Component/myDropdownlist.component.html",
        bindings: {
            list: "<",
            display: "<",
            value: "<",
            selectValue: "<",
            placeHolder: "<",
            defaultDisplay:"<",
            onSelectChange:"&"

        },
        controllerAs: "model",
        controller: ["$http", maincontroller]
    });
})();