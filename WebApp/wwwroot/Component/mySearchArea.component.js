﻿(function () {
    var module = angular.module('mySearchArea', ['myWaitCursor']);
    //var module = angular.module('appRegistration');
    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default:
                return false;
        }
    }
    function maincontroller($http) {
        var model = this;
        

        model.lists = [];
        model.UidSelected = null;
        model.SelectedList = [];
        model.loading = false;


        model.$onInit = function () {
            model.loading = false;
 
        };
        model.$onChanges = function () {
            model.loading = false;
        };
        model.SearchClick = function (value) {
            if (!empty(value)) {
                model.loading = true;
                $http.post(_getSearchArea, { "textsearch": value })
                    .then(function successCallback(response) {
                        model.lists = response.data;
                        model.loading = false;
                    }, function errorCallback(response) {
                        model.loading = false;
                    });
                
            }
        }
        
        model.selectlist = function (list) {
            model.UidSelected = list.UID;
            model.SelectedList = list;
        };


        model.CancelClick = function () {


        };
    }

    module.component("mySearchArea", {
        templateUrl: "/Component/mySearchArea.component.html",
        bindings: {
            textsearch:"<",
            okclick: '&'
        },
        controllerAs: "model",
        controller: ["$http", maincontroller]
    });
})();
