﻿(function () {
    "use strict";
    var module = angular.module('appRegistration');

    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default:
                return false;
        }
    }
    function getDomainData() {
        var val = {
            "domaincode": "VISTY,REFCAT,RPRIT"
            , "activeonly": "Y"
        };
        return val;
    }

    function maincontroller($http) {
        var model = this;

        model.lists = [];
        model.VISTY = [];
        model.REFCAT = [];
        model.RPRIT = [];
        model.Location = [];
        model.PatientPayor = [];

        model.$onInit = function () {

            $http.post(_getreference, getDomainData())
                .then(function (response) {
                    model.tblRefVal = response.data;
                    if (model.tblRefVal.length > 0) {
                        for (var i in model.tblRefVal) {
                            var str = model.tblRefVal[i]['DomainCode'];
                            model[str].push(model.tblRefVal[i]);
                        }
                    }
                });

            $http.post(_getLocationMaster, ({ "modeofuse": "REG", "entypuid": 1705 }))
                .then(function (response) {
                    model.Location = response.data;
                });

            $http.post(_getLastPayor, { "patientuid": model.patientuid })
                .then(function (Response) {
                    model.PatientPayor = Response.data;
                });

        };


    }

    module.component("myPatientVisit", {
        templateUrl: "/Views/ModuleRegister/myPatientVisit.component.html",
        bindings: {
            patientuid: "<"
            
        },
        controllerAs: "model",
        controller: ["$http", maincontroller]
    });
})();