﻿(function () {

    var app = angular.module('appRegistration');

    function verifyID(PID) {
        var digit = string.Empty;
        var sumValue = 0;
        for (var i = 0; i < PID.Length - 1; i++)
            sumValue += int.Parse(PID[i].toString()) * (13 - i);
        var v = 11 - (sumValue % 11);
        if (v.toString().Length === 2) {
            digit = v.toString().substring(1, 1);
        }
        else {
            digit = v.toString();
        }
        return PID[12].toString() === digit;
    }

    app.directive('myVerfifyid', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, mCtrl) {
                function myValidation(value) {
                    if (value.length === 13) {
                        mCtrl.$setValidity('ID', verifyID(value));
                    }
                    else if (value.length === 0) {
                        mCtrl.$setValidity('ID', true);
                    }
                    else {
                        mCtrl.$setValidity('ID', false);
                    }


                    return value;
                }

                mCtrl.$parsers.push(myValidation);
            }
        };
    });

})();



