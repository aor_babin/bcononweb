﻿(function () {

    var module = angular.module("appRegistration");

    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default:
                return false;
        }
    }
    function checkListExistValue(list, field, value) {
        for (var i = 0; i < list.length; i++) {
            if (list[i][field] == value) {
                return true;
            }
        }
        return false;
    }
    function dateToString(date) {
        var dd = ("0" + (date.getDate())).slice(-2);
        var mm = ("0" + (date.getMonth() + 1)).slice(-2);
        var yyyy = date.getFullYear().toString();
        return yyyy + "-" + mm + "-" + dd;
    }
    function getDomainData() {
        var val = {
            "domaincode": "PAYRTP"
            , "activeonly": "Y"
        };
        return val;
    }

    function getNewList() {
        var val = {
            "PatientVisitUID": 0,
            "PatientUID": 0,
            "PayorOfficeDetailUID": 0,
            "PayorOfficeDetailName": "",
            "PayorName": "",
            "PolicyMasterUID": 0,
            "PolicyName": "",
            "PayorAgreementName": "",
            "PlanNumber": "",
            "EligibileAmount": "",
            "Comments": "",
            "PayorUID": 0,
            "PAYRTPUID": 0,
            "Rank": "",
            "PayorAgreementUID": 0,
            "ActiveFrom": dateToString(new Date()),
            "ActiveTo": null,
            "ClaimPercentage": "0",
            "FixedCopayAmount": ""
        }
        return val;
    }

    function maincontroller($http) {
        var model = this;
        model.PAYRTP = [];
        model.Payor = [];
        model.PayorAgreement = [];
        model.SelectListItem = [];
        model.total = 3;
        model.selectItemIndex = -1;
        model.Payor = [];

        model.$onInit = function () {
            model.total = model.lists.length;

            $http.post(_getreference, getDomainData())
                .then(function (response) {
                    model.tblRefVal = response.data;
                    if (model.tblRefVal.length > 0) {
                        for (var i in model.tblRefVal) {
                            var str = model.tblRefVal[i]['DomainCode'];
                            model[str].push(model.tblRefVal[i]);
                        }
                    }
                });

            $http.post(_getPayor, { "Payornamesearch": ""})
                .then(function (response) {
                    model.Payor = response.data;
                });
        };

        model.onAdd_click = function () {
       
            model.lists.push(getNewList());
            model.selectlist(model.lists[model.lists.length - 1]);
        };
        model.onUpdate_click = function (value) {

            angular.merge(model.lists[model.selectItemIndex],value);
            
        };
        model.onDelete_click = function (value) {
            model.lists.splice(model.selectItemIndex, 1);
            SelectListItem = [];
            model.selectItemIndex = -1;
        };

        model.selectlist = function (value) {
            console.log(value);
            angular.copy(value, model.SelectListItem);
            model.selectItemIndex = model.lists.indexOf(value);
            model.formPayorDetail.$setValidity('Type', true);
        };

        model.onDateFrom_change = function (value) {
          
        };

        model.onDateTo_change = function (value) {

        };
        model.onselectRank_change = function (value,display) {
            //console.log(value, display);
            //console.log(model);
            if (checkListExistValue(model.lists, 'PAYRTPUID', value))
            {
                model.formPayorDetail.$setValidity('Type', false);
            }
            else
            {
                model.formPayorDetail.$setValidity('Type', true);
                model.SelectListItem.PAYRTPUID = value;
                model.SelectListItem.Rank = display;
           
            }
           
        };

        model.onselectPayorAgreement_change = function (value, display) {
            model.SelectListItem.PayorAgreementUID = value;
            model.SelectListItem.PayorAgreementName = display;
        };
        model.onselectPayor_change = function (value, display) {
            model.SelectListItem.PayorUID = value;
            model.SelectListItem.PayorName = display;
        };
    }

    module.component("myPayorDetail", {
        templateUrl: "/Views/ModuleRegister/myPayorDetail.component.html",
        bindings: {
            lists: "="
        },
        controllerAs: "model",
        controller: ["$http", maincontroller]
    });
})();