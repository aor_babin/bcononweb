﻿(function () {
    "use strict";
    var module = angular.module("appRegistration");

    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default:
                return false;
        }
    }
    function maincontroller($http) {
        var model = this;
        model.patient = {
            "UID": "",
            "PASID": "",
            "ForeName": "",
            "MiddleName": "",
            "SurName": "",
            "BirthDate": "",
            "Telephone":""
        };
        model.lists = [];
        model.UidSelected = null;
        model.SelectedList = [];
        model.loading = false;

        model.$onInit = function () {
            model.loading = false;
        };

        
        model.ClearClick = function () {
            model.patient = [];
            model.lists = [];
           
        };

        model.SearchClick = function () {
            model.SelectedList = model.patient;

            if (!empty(model.patient.PASID) ||
                !empty(model.patient.ForeName) ||
                !empty(model.patient.MiddleName) ||
                !empty(model.patient.SurName) ||
                !empty(model.patient.BirthDate) ||
                !empty(model.patient.Telephone)) {
                model.loading = true;
                $http.post(_getSearchPatient, {
                    "hn": model.patient.PASID,
                    "forename": model.patient.ForeName,
                    "middlename": model.patient.MiddleName,
                    "surname": model.patient.SurName,
                    "phone": model.patient.Telephone,
                    "dob": model.patient.BirthDate
                }).then(function successCallback(response) {
                    model.lists = response.data;
                    model.loading = false;
                }, function errorCallback(response) {
                    model.loading = false;
                });
                
            }
            else {
                model.lists = [];
            }
        };
        model.selectlist = function (list) {
            model.SelectedList = list;
            model.UidSelected = list.UID;
        };

        //model.okclick = function () {
        //    model.isopen = false;
        //};

        model.CancelClick = function () {
            model.patient = [];
          
        };
        
    }

    module.component("mySearchPatient", {
        templateUrl: "/Views/ModuleRegister/mySearchPatient.component.html",
        bindings: {

            okclick :'&'
        },
        controllerAs: "model",
        controller: ["$http", maincontroller]
    });
})();
