﻿(function () {
    var module = angular.module('appRegistration');

    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default:
                return false;
        }
    }

    function maincontroller($http) {
        var model = this;
        model.total = 0;
        model.lists = [];

        //{
        //    "UID": 1695872,
        //        "SlotStartDttm": "10/12/2017",
        //            "CareProvider": "Emergency Service OPD",
        //                "Location": "พยาบาล ER",
        //                    "CareProviderUID": 1944,
        //                        "LocationUID": 65
        //}

        model.$onInit = function () {
            if (!empty(model.patientuid)) {
                $http.post(_getPatientAppointment, { "patientuid": model.patientuid })
                    .then(function successCallback(response) {
                        model.lists = response.data;
                        model.total = model.lists.length;
                    }, function errorCallback(response) {
                        model.total = 0;
                    });
            } 
        };

        model.$onChanges = function () {
            if (!empty(model.patientuid)) {
                $http.post(_getPatientAppointment, { "patientuid": model.patientuid })
                    .then(function successCallback(response) {
                        model.lists = response.data;
                        model.total = model.lists.length;
                    }, function errorCallback(response) {
                        model.total = 0;
                    });
            }
        };
    }
    
    module.component("myAppointmentList", {
        templateUrl:"/Views/ModuleRegister/myAppointmentList.component.html",
        bindings: {
            patientuid:"<"
        },
        controllerAs: "model",
        controller: ["$http",maincontroller]

    });

})();