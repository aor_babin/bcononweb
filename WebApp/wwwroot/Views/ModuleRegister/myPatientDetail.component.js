﻿(function () {
    "use strict";
    var module = angular.module("appRegistration");

    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default:
                return false;
        }
    }

    function dateToString(date) {
        var dd = ("0" + (date.getDate())).slice(-2);
        var mm = ("0" + (date.getMonth() + 1)).slice(-2);
        var yyyy = date.getFullYear().toString();
        return yyyy + "-" + mm + "-" + dd;
    }

    function getAge(date1, date2) {
        var y = date1.getFullYear() - date2.getFullYear();
        var m = date1.getMonth() - date2.getMonth();
        var d = date1.getDate() - date2.getDate();
        

       
        if (d < 0) {
            m = m - 1;
            d = d + 30;
        }
       if (m <0) {
            y = y - 1;
            m = m + 12;
        }
        if (y < 0) {
            y = 0;
        }
        var val = {
            'ageYear': y,
            'ageMonth': m,
            'ageDay': d,
            'ageString': y.toString() + " Y " + m.toString() + " M " + d.toString() + " D"
        };
        return val;
        //return;
    }

    

    function verifyID(PID) {
        var digit = null;
        var sumValue = 0;
        for (var i = 0; i < PID.length - 1; i++)
            sumValue += parseInt(PID[i].toString()) * (13 - i);
        var v = 11 - (sumValue % 11);
        if (v.toString().length == 2) {
            digit = v.toString().substring(1, 1);
        }
        else {
            digit = v.toString();
        }
        return PID[12].toString() === digit;
    };

    function getDomainData() {
        var val = {
            "domaincode": "TITLE,SEXXX,BLOOD,SPOKL,NATNL,ETHGP,RELGN,VIPTP,RNEWS,MARRY,OCCUP"
            , "activeonly": "Y"
        };
        return val;
    }
    function getAddress() {
        var val = {
            "UID": "",
            "Line1": "",
            "Line2": "",
            "Line3": "",
            "Line4": "",
            "STATEUID": "",
            "CITTYUID": "",
            "AREAUID": ""
        };
        return val;
    }
    function getPatientAlias() {
        var val = {
            "UID": 0,
            "ForeName": "",
            "MiddleName": "",
            "SurName": "",
            "TITLEUID": 0,
            "PatientUID": 0,
            "PAALIUID": "4546"  // english name
        };
        return val;
    }

    function getFormConfig() {
        var val = {
            "TITLE": false,
            "ForeName": true,
            "SurName": false,
            "DateOfBirth": false,
            "Gender": false,
            "Nation": false,
            "PreferLanguage": false,
            "ResidencePhone": false,
            "MobilePhone": false,
            "AddressNO": false,
            "ForeNameENG": false,
            "SurNameENG": false,
            "TitleENG": false
        };
        return val
    }

    function getContact() {
        var val = {

            "Phone": "",
            "Mobile": "",
            "Email": ""
        };
        return val;
    }
    function getPatientID() {
        var val = {
            "ID": null,
            "DateFrom": null,
            "DateTo": null,
            "Passport": null
        }
        return val;
    }
    function getPatient() {
        var val = {

            "UID": 0,
            "PASID": "",
            "Surname": "",
            "Forename": "",
            "middleName": "",
            "TitleUID": 0,
            "NATNLUID": 0,
            "SPOKLUID": 0,
            "DOBComputed": "0",
            "BirthDate": null,
            "Age": "",
            "SEXXXUID": 0,
            "MARRYUID": 0,
            "OCUPATIONUID": 0,
            "BLOODUID": 0,
            "OCUPATION": "",
            "BloodGroup": "",
            "line1": "",
            "line2": "",
            "line3": "",
            "line4": "",
            "pincode": "",
            "STATE": "",
            "Country": "",
            "City": "",
            "Area": "",
            "MWHEN": "",
            "STATEUID": 0,
            "CNTRYUID": 0,
            "CITTYUID": 0,
            "AREAUID": 0,
            "ENGUID": 0,
            "SurnameENG": "",
            "ForenameENG": "",
            "middleNameENG": "",
            "TitleENGUID": 0,
            "PatientImage": null

        }
        return val;
    }

    function maincontroller($http) {
        var model = this;

        model.lists = [];
        model.message = [];


        model.TITLE = [];
        model.SEXXX = [];
        model.BLOOD = [];
        model.SPOKL = [];
        model.NATNL = [];
        model.ETHGP = [];
        model.RELGN = [];
        model.VIPTP = [];
        model.IsAnonymous = [];
        model.SEQName = [];
        model.RNEWS = [];
        model.MARRY = [];
        model.OCCUP = [];
        model.tblRefVal = [];


        model.Patient = [];
        model.PatientAddress = [];
        model.PatientEngName = [];
        model.Config = [];
        model.PatientContact = [];
        model.PatientID = [];
        model.btnSaveDisable = false;


        model.$onInit = function () {
        
            $http.post(_getreference, getDomainData())
                .then(function (response) {
                    model.tblRefVal = response.data;
                    if (model.tblRefVal.length > 0) {
                        for (var i in model.tblRefVal) {
                            var str = model.tblRefVal[i]['DomainCode'];
                            model[str].push(model.tblRefVal[i]);
                        }
                    }
                });

            $http.get(_getformconfig)
                .then(function (response) {
                    var obj = response.data;
                    if (obj.length > 0) {
                        for (var i in obj) {
                            model.Config[obj[i]['Name']] = obj[i]['IsRequired'];
                        }
                    }

                });

        };

        model.$onChanges = function () {


        };

        model.$postLink = function () {
            //console.log('mypatientdetail');
            model.Patient = getPatient();
            //model.PatientAddress = getAddress();
            //model.PatientEngName = getPatientAlias();
            model.Config = getFormConfig();
            model.PatientContact = getContact();
            model.PatientID = getPatientID();

        };


        model.SearchPatientByHN = function (value) {
            if (!empty(value)) {
                if (value.length == 10) {
                    var str = value.split("");
                    value = str[0] + str[1] + "-" + str[2] + str[3] + "-" + str[4] + str[5] + str[6] + str[7] + str[8] + str[9];

                }
                else if (value.length !== 12) {
                    return;
                }

                $http.post(_getPatient, { "pasid": value })
                    .then(function (response) {
                        model.Patient = response.data[0];
                        model.onPatientChange({ "uid": model.Patient.UID });
                        model.btnSaveDisable = true;
                        model.onselectDOB_change(model.Patient.BirthDate);
                        
                    }
                    );
            }

        }
        model.saveOnclick = function () {

        };

        model.updateOnclick = function () {

        };

        model.clearOnclick = function () {

            model.Patient = getPatient();
        };

        model.regOnclick = function () {
           

            //return { "patientuid": model.Patient.UID };
        }

        // varidate method
        model.ID_verifying = function (value) {

            if (value.length == 13) {
                model.formPatientDetail.inputID.$setValidity('ID', verifyID(value));
            }
            else if (value.length == 0) {
                model.formPatientDetail.inputID.$setValidity('ID', true);
            }
            else {

                model.formPatientDetail.inputID.$setValidity('ID', false);
            }

        };

        model.AgeComputation = function (value) {
            if (empty(value)){
                return;
            }
            var date = new Date();
            var mydate = new Date((date.getFullYear() - value).toString());

            var val = getAge(date, mydate);
            model.Patient.Age = val.ageString;
            model.Patient.BirthDate = dateToString(mydate);
            model.Patient.DOBComputed = '1';
        };

        
        // method from child component
        model.SearchPatientOkclick = function (value) {
            model.Patient = [];
            if (!empty(value.UID)) {
                $http.post(_getPatient, { "patientuid": value.UID })
                    .then(function (response) {
                        model.Patient = response.data[0];
                        console.log(model.Patient);
                        model.onPatientChange({ "uid": model.Patient.UID });
                        model.onselectDOB_change(model.Patient.BirthDate);
                    });
            }
            else {
                model.Patient = value;

            }
        };

        model.SearchAreaOkClick = function (value) {
            angular.merge(model.Patient,value)
            //model.PatientAddress = value;

        };

        model.PhotoSaveclick = function (value) {
            var str = value.replace("data:image/jpeg;base64,", "");
            model.Patient.PatientImage = str;
        };
        model.registerOnclick = function () {

        };

        model.onselectDOB_change = function (value) {
            
            var date1 = new Date();
            var date2 = new Date(value);
            var val = getAge(date1, date2);
            model.Patient.Age = val.ageString;
            model.Patient.AgeNum = val.ageYear;
            model.Patient.DOBComputed = '0';
        };

        model.onselectSEQName_change = function (value) {
            model.Patient.SEQName = value;
        };
        model.onselectTITLE_change = function (value) {
            model.Patient.titleUID = value;
        };
        model.onselectTITLEEngName_change = function (value) {
            model.Patient.TitleEngUID = value;
        }
        model.onselectSEXXX_change = function (value) {
            model.Patient.SEXXXUID = value;
        };
        model.onselectBLOOD_change = function (value) {
            model.Patient.BLOODUID = value;
        };
        model.onselectSPOKL_change = function (value) {
            model.Patient.SPOKLUID = value;
        };
        model.onselectNATNL_change = function (value) {
            model.Patient.NATNLUID = value;
        };
        model.onselectETHGP_change = function (value) {
            model.Patient.ETHGPUID = value;
        };
        model.onselectRELGN_change = function (value) {
            model.Patient.RELGNUID = value;
        };
        model.onselectMARY_change = function (value) {
            model.Patient.MARRYUID = value;
        };
        model.onselectVIPTP_change = function (value) {
            model.Patient.VIPTPUID = value;
        };
        model.onselectRNEWS_change = function (value) {
            model.Patient.RNEWS = value;
        };
        model.onselectOCCUP_change = function (value) {
            model.Patient.OCUPATIONUID = value;
        };
    }

    module.component("myPatientDetail", {
        templateUrl: "/Views/ModuleRegister/myPatientDetail.component.html",
        bindings: {
            patientuid: "<",
            onPatientChange:"&"
        },
        controllerAs: "model",
        controller: ["$http", maincontroller]
    });
})();