﻿(function () {

    var module = angular.module('appRegistration', ['ngRoute'
        ,'myDatetimepicker'
        ,'myDropdownList'
        , 'mySearchArea'
        , 'myCamera'
        ,'componentTest'
    ]);

    module.controller("appController", function ($scope, $http) {
  
        $scope.uid = "";
   
        $scope.Patient = [];
        $scope.Patient = {
            "uid": 105434,
            "name": 'test'

        };
        $scope.payorlist = [];
        $http.post(_getLastPayor, { "patientuid": 105434 })
            .then(function (Response) {
                $scope.payorlist = Response.data;
            });
        
    
        $scope.SearAreaOkclick = function (value) {
            $scope.area = value;
        };

        $scope.SearPatientOkclick = function (value) {
            $scope.psearch = value;
        };
        $scope.Photookclick = function (value) {
            $scope.Photo = value;
        }


        $scope.patientdetailchange = function (value) {
            $scope.Patient.uid = value;  
        };

        $scope.ontest_click = function () {
            alert("from test btn");
        };
    });

    module.config(function ($routeProvider) {
        $routeProvider.when("/patientdetail", {
            template: '<my-patient-detail patientuid="Patient.uid" on-patient-change="patientdetailchange(uid)"></my-patient-detail>'
        })
            .when("/patientvisit", {
                template: '<my-patient-visit patientuid="Patient.uid"></my-patient-visit>'
            })
            .otherwise({ redirectTo: "/patientdetail" });
    });
   
})();