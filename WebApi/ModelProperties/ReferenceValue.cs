﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProperties
{
    public class ReferenceValue
    {
        public string UID { get; set; }
        public string ValueCode { get; set; }
        public string Description { get; set; }
        public string other { get; set; }
        public Boolean IsChecked { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
    public class RoleDataLocation
    {
        public int RoleUID { get; set; }
        public int LocationUID { get; set; }
        public string Description { get; set; }
    }
    public class ParameterData
    {
        public int PatientUID { get; set; }
        public int PatientVisitUID { get; set; }
        public int GroupUID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int RoleUID { get; set; }
        public int UserUID { get; set; }
        public int LoginUID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
