﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProperties
{
    public class Patient
    {
        public int UID { get; set; }
        public string HN { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
    }
    public class PatientVisit
    {
        public int UID { get; set; }
        public string EN { get; set; }
        public Patient Patients { get; set; }
    }

    public class ModelAdmission
    {
        public int UID { get; set; }
        public int PatientUID { get; set; }
        public string HN { get; set; }
        public string EN { get; set; }
        public string PatientName { get; set; }
        public string LocationName { get; set; }
        public string BedName { get; set; }
        public string CareProviderName { get; set; }
        public string Visitdate { get; set; }
        public string Status { get; set; }

    }


}
