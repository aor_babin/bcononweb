﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProperties
{
 
    public class SearchOrderEntry
    {
        public string UID { get; set; }
        public string Code { get; set; }
        public string ItemName { get; set; }
        public string CategoryName { get; set; }
        public string SubCategroyName { get; set; }
        public string TypeOrder { get; set; }
        public string TypeItem { get; set; }
        public string ColorConvert { get; set; }
        public string BSMDDUID { get; set; }
        public int grouporder { get; set; }
        public string InFixPrice { get; set; }
        public string IsUseFixPrice { get; set; }
        public string OrderSetIsFixPrice { get; set; }
        public string GenericName { get; set; }
        public string aliasname { get; set; }
        public string ItemNameSearch { get; set; }
        public string CodeSame { get; set; }
        public string ItemNameSame { get; set; }
        public int orderby { get; set; }
        public int Afarbet { get; set; }
        public string InSSO { get; set; }
        public string IsUseSSO { get; set; }
        public int SSOBillableitemUID { get; set; }
        public string SSOBillableitemName { get; set; }
        public int OrderSubCategoryUID { get; set; }


    }
    public class OrderRecent
    {
        public int BillabliItemUID { get; set; }
        public int CountRecent { get; set; }
    }

    public class SearchOrderParameter
    {
        public string SearchText { get; set; }
        public List<SearchOrderEntry> data { get; set; }
    }


}
