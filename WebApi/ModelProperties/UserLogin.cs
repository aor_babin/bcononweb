﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProperties
{
    public class UserLogin
    {
        public int LoginUID { get; set; }
        public int UserUID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int RoleUID { get; set; }
        public int LocationUID { get; set; }
    }
}
