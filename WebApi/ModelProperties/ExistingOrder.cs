﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProperties
{
    public class ExistingOrderModel
    {
        public string OrderDate { get; set; }
        public string Status { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Frequency { get; set; }
        public float Quantity { get; set; }
        public string UomUnit { get; set; }
        public double Dosage { get; set; }
        public string DosageUnit { get; set; }
        public string Comments { get; set; }
        public double NetAmount { get; set; }
        
    }
    
}
