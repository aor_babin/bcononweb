﻿using ModelProperties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Configuration;

namespace WebApi.Models
{
    public class FileDatahelper
    {
        string _path = string.Empty;
        string _pathCareprovider = string.Empty;
        string _pathSearchOrder = string.Empty;
        string _pathReferenceValue = string.Empty;
        string _pathMeal = string.Empty;
        string _pathDuplicateAddOrder = string.Empty;
        string _pathCateNoAlertDupicate = string.Empty;
        string _pathItemForDuplicateOrderInGrid = string.Empty;
        string _pathFrequencyDefinition = string.Empty;
        string _pathLocation = string.Empty;
        string _pathProblem = string.Empty;

        public FileDatahelper()
        {
            //_path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"\BConnectReport\LoadMaster\MasterData\";
            _path = ConfigurationManager.AppSettings["PathXml"].ToString() + "//";
            _pathCareprovider = _path + "Careprovider.xml";
            _pathSearchOrder = _path + "SearchOrder.xml";
            _pathReferenceValue = _path + "ReferenceValue.xml";
            _pathMeal = _path + "Meal.xml";
            _pathDuplicateAddOrder = _path + "DuplicateAddOrder.xml";
            _pathCateNoAlertDupicate = _path + "CateNoAlertDupicate.xml";
            _pathItemForDuplicateOrderInGrid = _path + "ItemForDuplicateOrderInGrid.xml";
            _pathFrequencyDefinition = _path + "FrequencyDefinition.xml";
            _pathLocation = _path + "Location.xml";
            _pathProblem = _path + "Problem.xml";
        }
        public List<SearchOrderEntry> getSearchOrderFile()
        {
            List<SearchOrderEntry> _data = new List<SearchOrderEntry>();
            if (File.Exists(_pathSearchOrder))
            {
                XElement xmlTree2 = XElement.Load(_pathSearchOrder, LoadOptions.None);
                _data = xmlTree2.Elements("Setup")
                                   .Select(el => new SearchOrderEntry()
                                   {
                                       UID = el.Attribute("UID").Value,
                                       Code = el.Attribute("Code").Value,
                                       ItemName = el.Attribute("ItemName").Value,
                                       CategoryName = el.Attribute("CategoryName").Value,
                                       SubCategroyName = el.Attribute("SubCategroyName").Value,
                                       TypeOrder = el.Attribute("TypeOrder").Value,
                                       TypeItem = el.Attribute("TypeItem").Value,
                                       ColorConvert = el.Attribute("ColorConvert").Value,
                                       BSMDDUID = el.Attribute("BSMDDUID").Value,
                                       grouporder = int.Parse(el.Attribute("grouporder").Value),
                                       IsUseFixPrice = el.Attribute("IsUseFixPrice").Value,
                                       OrderSetIsFixPrice = el.Attribute("OrderSetIsFixPrice").Value,
                                       GenericName = el.Attribute("GenericName").Value,
                                       aliasname = el.Attribute("aliasname").Value,
                                       ItemNameSearch = el.Attribute("ItemNameSearch").Value,
                                       CodeSame = el.Attribute("CodeSame").Value,
                                       ItemNameSame = el.Attribute("ItemNameSame").Value,
                                       orderby = int.Parse(el.Attribute("orderby").Value),
                                       Afarbet = int.Parse(el.Attribute("Afarbet").Value),
                                       OrderSubCategoryUID = int.Parse(el.Attribute("OrderSubCategoryUID").Value)
                                   }
                                   )
                                   .ToList();
            }
            return _data;
        }
        public List<ReferenceValue> getCareProviderFile()
        {
            string Success = string.Empty;
            List<ReferenceValue> _data = new List<ReferenceValue>();
            if (File.Exists(_pathCareprovider))
            {
                XElement xmlTree2 = XElement.Load(_pathCareprovider, LoadOptions.None);
                _data = xmlTree2.Elements("Setup")
                                   .Select(el => new ReferenceValue()
                                   {
                                       UID = el.Attribute("UID").Value,
                                       ValueCode = el.Attribute("Code").Value,
                                       Description = el.Attribute("Name").Value,
                                   }
                                   )
                                   .ToList();
            }
            return _data;
        }
        public List<ReferenceValue> getLocation()
        {
            List<ReferenceValue> _data = new List<ReferenceValue>();
            if (File.Exists(_pathLocation))
            {
                XElement xmlTree2 = XElement.Load(_pathLocation, LoadOptions.None);
                _data = xmlTree2.Elements("Setup")
                                   .Select(el => new ReferenceValue()
                                   {
                                       UID = el.Attribute("UID").Value,
                                       Description = el.Attribute("Name").Value,
                                   }
                                   )
                                   .ToList();
            }
            return _data;
        }
    }
}