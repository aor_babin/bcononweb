﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using ModelProperties;
using System.Globalization;
using System.Threading;

namespace WebApi.Models
{
    public class dbhelper
    {
        SqlConnection conn;
        FileDatahelper ff = new FileDatahelper();
        public dbhelper()
        {
            string _con = ConfigurationManager.AppSettings["Connection"].ToString();
            conn = new SqlConnection(_con);
        }
        void Connect()
        {
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
        }
        void DisConnect()
        {
            if (conn.State == System.Data.ConnectionState.Open)
            {
                conn.Close();
            }
        }
        public ModelAdmission getPatient(PatientVisit data)
        {
            ModelAdmission ls = new ModelAdmission();
            DataTable Dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from vbdmsadmission where UID=@UID";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@UID", data.UID);
                Dt.Load(cmd.ExecuteReader());
                if (Dt.Rows.Count > 0)
                {
                    ls = Dt.AsEnumerable().Select(p => new ModelAdmission()
                    {
                        UID = int.Parse(p["UID"].ToString()),
                        PatientName = p["PatientName"].ToString(),
                        EN = p["EpisodeNumber"].ToString(),
                        HN = p["PatientID"].ToString(),
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

            }
            DisConnect();
            cmd.Dispose();

            return ls;
        }
        public ExistingOrderModel[] getBDMSPatientOrderByVisit(int PatientUID, int PatientVisitUID,DateTime startdate,DateTime enddate,int groupuid,int Roleuid)
        {
            DataTable Dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 10000;
                cmd.CommandText = "pBDMSGetPatientOrderByVisit";
                cmd.Parameters.AddWithValue("@P_PatientUID", PatientUID);
                cmd.Parameters.AddWithValue("@P_PatientVisitUID", PatientVisitUID);
                cmd.Parameters.AddWithValue("@P_FromDttm", startdate);
                cmd.Parameters.AddWithValue("@P_ToDttm", enddate);
                cmd.Parameters.AddWithValue("@P_ORDCTUID", groupuid);
                cmd.Parameters.AddWithValue("@P_OrderSubCategory", 0);
                cmd.Parameters.AddWithValue("@P_ItemUID", 0);
                cmd.Parameters.AddWithValue("@P_ORDSTUID", 0);
                cmd.Parameters.AddWithValue("@P_Billed", 'N');
                cmd.Parameters.AddWithValue("@P_StartRow", 0);
                cmd.Parameters.AddWithValue("@P_EndRow", 0);
                cmd.Parameters.AddWithValue("@P_WardLocationUID", DBNull.Value);
                cmd.Parameters.AddWithValue("@P_ENTYPUID", DBNull.Value);
                cmd.Parameters.AddWithValue("@P_OrderByUID", DBNull.Value);
                cmd.Parameters.AddWithValue("@P_OrderToLocationUID", DBNull.Value);
                cmd.Parameters.AddWithValue("@ROLEUID", Roleuid);
                Dt.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {

            }
            DisConnect();
            
            var IE = Dt.AsEnumerable().Select(p => new ExistingOrderModel()
            {
                OrderDate = DateTime.Parse(p["OrderDate"].ToString()).ToString("dd/MM/yyyy HH:mm"),
                Status = p["Status"].ToString(),
                ItemCode = p["ItemCode"].ToString(),
                ItemName = p["OrderItemList"].ToString(),
                Frequency = p["Frequency"].ToString(),
                Quantity = (p["Quantity"].ToString() == "" ? 0 : float.Parse(p["Quantity"].ToString())),
                UomUnit = p["UomUnit"].ToString(),
                Dosage = (p["Dosage"].ToString() == "" ? 0 : Double.Parse(p["Dosage"].ToString())),
                DosageUnit = p["DosageUnit"].ToString(),
                Comments = p["Comments"].ToString(),
                NetAmount = (p["NetAmount"].ToString() == "" ? 0 : Double.Parse(p["NetAmount"].ToString())),

                //PatientName = p["PatientName"].ToString(),
                //PatientID = p["PatientID"].ToString(),
                //Ward = p["Ward"].ToString(),
                //Location = p["Location"].ToString(),
                //Bed = p["Bed"].ToString(),
                //BedCategory = p["BedCategory"].ToString(),
                //OrderNumber = p["OrderNumber"].ToString(),
                //
                //OrderCategory = p["OrderCategory"].ToString(),
                //
                //Priority = p["Priority"].ToString(),
                //StartDttm = DateTime.Parse(p["StartDttm"].ToString()),
                //EndDttm = (p["EndDttm"].ToString() == "" ? x : DateTime.Parse(p["EndDttm"].ToString())),
                //IdentifyingUID = (p["IdentifyingUID"].ToString() == "" ? 0 : int.Parse(p["IdentifyingUID"].ToString())),
                //IdentifyingType = p["IdentifyingType"].ToString(),
                //RequestedBy = p["RequestedBy"].ToString(),
                //OrderUID = (p["OrderUID"].ToString() == "" ? 0 : int.Parse(p["OrderUID"].ToString())),
                //OrderItemList = p["OrderItemList"].ToString(),
                //OrderStatus = p["OrderStatus"].ToString(),
                //LabPresNumber = p["LabPresNumber"].ToString(),

                //PackageName = p["PackageName"].ToString(),
                //PatientOrderDetailUID = (p["PatientOrderDetailUID"].ToString() == "" ? 0 : long.Parse(p["PatientOrderDetailUID"].ToString())),
                //Payor = p["Payor"].ToString(),
                //PaymentStatus = p["PaymentStatus"].ToString(),
                //OrderLocationUID = (p["OrderLocationUID"].ToString() == "" ? 0 : int.Parse(p["OrderLocationUID"].ToString())),
                //OrderToLocationUID = (p["OrderToLocationUID"].ToString() == "" ? 0 : int.Parse(p["OrderToLocationUID"].ToString())),
                //OrderFromLocation = p["OrderFromLocation"].ToString(),
                //OrderToLocation = p["OrderToLocation"].ToString(),
                //Comments = p["Comments"].ToString(),
                //
                //IsStandingOrder = (p["IsStandingOrder"].ToString() == "" ? 'N' : char.Parse(p["IsStandingOrder"].ToString())),
                //ExecutedBy = p["ExecutedBy"].ToString(),
                //ExecutedGroup = p["ExecutedGroup"].ToString(),
                //TotalRowCount = (p["TotalRowCount"].ToString() == "" ? 0 : int.Parse(p["TotalRowCount"].ToString())),
                //CancelledByUser = p["CancelledByUser"].ToString(),
                //CancelledDttm = (p["CancelledDttm"].ToString() == "" ? x : DateTime.Parse(p["CancelledDttm"].ToString())),
                //BillPackageUID = (p["BillPackageUID"].ToString() == "" ? 0 : int.Parse(p["BillPackageUID"].ToString())),
                //Reason = p["Reason"].ToString(),

                //DoctorShare = (p["DoctorShare"].ToString() == "" ? 0 : Double.Parse(p["DoctorShare"].ToString())),
                //OrderCategoryUID = (p["OrderCategoryUID"].ToString() == "" ? 0 : int.Parse(p["OrderCategoryUID"].ToString())),
                //OrderSubCategoryUID = (p["OrderSubCategoryUID"].ToString() == "" ? 0 : int.Parse(p["OrderSubCategoryUID"].ToString())),
                //PatientUID = (p["PatientUID"].ToString() == "" ? 0 : long.Parse(p["PatientUID"].ToString())),
                //PatientVisitUID = (p["PatientVisitUID"].ToString() == "" ? 0 : int.Parse(p["PatientVisitUID"].ToString())),
                //ItemUID = (p["ItemUID"].ToString() == "" ? 0 : int.Parse(p["ItemUID"].ToString())),
                //ReferenceNumber = p["ReferenceNumber"].ToString(),
                //IsExecutable = char.Parse(p["IsExecutable"].ToString()),
                //
                //InstructionText = p["InstructionText"].ToString(),
                //ParentOrdNumber = p["ParentOrdNumber"].ToString(),
                //IsHourlyCharge = char.Parse(p["IsHourlyCharge"].ToString()),
                //AltBillableItemUID = (p["AltBillableItemUID"].ToString() == "" ? 0 : int.Parse(p["AltBillableItemUID"].ToString())),
                //ORDSTUID = (p["ORDSTUID"].ToString() == "" ? 0 : int.Parse(p["ORDSTUID"].ToString())),
                //AltBillableItemName = p["AltBillableItemName"].ToString(),
                //CoderComments = p["CoderComments"].ToString(),
                //CoderStatusUID = (p["CoderStatusUID"].ToString() == "" ? 0 : int.Parse(p["CoderStatusUID"].ToString())),
                //AltBillableItemCode = p["AltBillableItemCode"].ToString(),
                //PackOrSet = p["PackOrSet"].ToString(),
                //
                //
                //IsDF = p["IsDF"].ToString(),
                //IsBillableEvent = p["IsBillableEvent"].ToString(),
                //ItemCode = p["ItemCode"].ToString(),
                //BillableItemUID = p["BillableItemUID"].ToString(),
                //IsFixPrice = p["IsFixPrice"].ToString(),
                //MainFixPrice = (p["MainFixPrice"].ToString() == "Y" ? true : false),
                //IsJobMedStanding = p["IsJobMedStanding"].ToString(),
                //CategoryCode = p["CategoryCode"].ToString(),
                //SubCategoryCode = p["SubCategoryCode"].ToString(),
                //IsMBCode = p["IsMBCode"].ToString(),
                //IsNursingAlert = p["IsNursingAlert"].ToString(),
                //IsNursingAlertFlag = p["IsNursingAlertFlag"].ToString(),
                //IsCanFontWeight = (p["IsNursingAlert"].ToString() == "Y" ? (p["IsNursingAlertFlag"].ToString() == "Y" ? "N" : "Y") : "N"),
                //IsHaveEndDate = (p["EndDttm"].ToString() == "" ? "N" : "Y"),
                //IsOrderAlert = p["IsOrderAlert"].ToString(),
                //IsUseSSO = p["IsUseSSO"].ToString(),
                //IsSSOAlert = p["IsOrderAlertSSO"].ToString(),
                //OrderAlertALGNTSOSUID = p["OrderAlertALGNTSOSUID"].ToString(),
                //OrderAlertSSORemarks = p["OrderAlertSSORemarks"].ToString(),
            }
            ).ToArray().ToArray();
            return IE;
        }
        public List<ReferenceValue> getBDMSGroupCategory()
        {
            List<ReferenceValue> ls = new List<ReferenceValue>();
            DataTable Dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from BDMSGroupCategory where statusflag='A' order by GroupName";
                cmd.Parameters.Clear();
                Dt.Load(cmd.ExecuteReader());
                if (Dt.Rows.Count > 0)
                {
                    ls = Dt.AsEnumerable().Select(p => new ReferenceValue()
                    {
                        UID = p["UID"].ToString(),
                        Description = p["Groupname"].ToString(),
                    }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            DisConnect();
            cmd.Dispose();

            return ls;
        }
        public List<ReferenceValue> getVisitAll(int PatientUID)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable Dt = new DataTable();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "pBDMSGetVisitAllExistingOrder";
                cmd.Parameters.AddWithValue("@P_PatientUID", PatientUID);
                Dt.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {

            }
            DisConnect();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var IE = Dt.AsEnumerable().Select(p => new ReferenceValue()
            {
                UID = p["UID"].ToString(),
                Description = p["Name"].ToString(),
                ValueCode = p["DateOrder"].ToString(),
                StartDate = DateTime.Parse(p["StartDTTM"].ToString()).ToString("dd/MM/yyyy"),
                EndDate = DateTime.Parse(p["EndDttm"].ToString()).ToString("dd/MM/yyyy"),
            }
            ).ToList();
            return IE;
        }
        public List<SearchOrderEntry> getSearchOrderAll(int VISITUID,int RoleUID,int UserUID)
        {
            List<int> ls = new List<int>();
            string Sql = @"SELECT distinct RC.OrderSubCategoryUID  FROM RoleOrderPermission RP , RoleOrderCategroy RC      
                 WHERE RP.UID = RC.RoleOrderPermissionUID      
                 AND RP.StatusFlag ='A'      
                 AND RC.StatusFlag ='A'      
                 AND RP.RoleUID = " + RoleUID.ToString() + " and RC.OrderSubCategoryUID is not null";
       
            SqlCommand cmd = new SqlCommand();
            DataTable Dt = new DataTable();
            DataTable Dt1 = new DataTable();
            DataTable Dt2 = new DataTable();
            List<OrderRecent> ll = new List<OrderRecent>();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = Sql;
                Dt.Load(cmd.ExecuteReader());

                if (Dt.Rows.Count > 0)
                {
                    ls = Dt.AsEnumerable().Select(p => int.Parse(p["OrderSubCategoryUID"].ToString())).ToList();
                }

                Sql = "select top 1 isnull(IsUseSearchOrderOrderbyRecent,'N') from BillConfiguration where StatusFlag='A'";
                cmd.CommandText = Sql;
                Dt1.Load(cmd.ExecuteReader());
                if (Dt1.Rows[0][0].ToString() == "Y")
                {
                    Sql = @"select * from BDMSOrderRecent where Cuser=" + UserUID.ToString();
                    cmd.CommandText = Sql;
                    Dt2.Load(cmd.ExecuteReader());
                    ll = Dt2.AsEnumerable().Select(p => new OrderRecent()
                    {
                        BillabliItemUID = int.Parse(p["BillableItemUID"].ToString()),
                        CountRecent = int.Parse((p["CountRecent"].ToString() == "" ? "0" : p["CountRecent"].ToString())),
                    }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            if (conn.State == System.Data.ConnectionState.Open)
            {
                conn.Close();
            }
            cmd.Dispose();

            List<SearchOrderEntry> xx = ff.getSearchOrderFile();
            List<SearchOrderEntry> xx1 = xx.Where(p => ls.Contains(p.OrderSubCategoryUID)).ToList();

            foreach (OrderRecent x in ll)
            {
                foreach (SearchOrderEntry item in xx1.Where(p => p.UID == x.BillabliItemUID.ToString()))
                {
                    item.grouporder = x.CountRecent;
                }
            }

            return xx1;
        }
        public List<SearchOrderEntry> SearchInClient(List<SearchOrderEntry> _dataSearchAll, string _searchtext)
        {
            string P_NameOri = _searchtext;
            string P_Name = _searchtext;
            P_Name = P_Name.Replace(",", "");
            P_Name = P_Name.Replace(".", "");
            P_Name = P_Name.Replace("(", "");
            P_Name = P_Name.Replace(")", "");
            P_Name = P_Name.Replace(";", "");
            P_Name = P_Name.Replace("#", "");
            P_Name = P_Name.Replace("<", "");
            P_Name = P_Name.Replace(">", "");
            P_Name = P_Name.Replace("?", "");
            P_Name = P_Name.Replace(@"""", "");
            P_Name = P_Name.Replace("*", "");
            P_Name = P_Name.Replace("&", "");
            P_Name = P_Name.Replace("^", "");
            P_Name = P_Name.Replace("$", "");
            P_Name = P_Name.Replace("@", "");
            P_Name = P_Name.Replace("!", "");
            P_Name = P_Name.Replace("|", "");
            P_Name = P_Name.Replace("}", "");
            P_Name = P_Name.Replace("{", "");
            P_Name = P_Name.Replace(":", "");
            P_Name = P_Name.Replace(@"\", "");
            P_Name = P_Name.Replace("/", "");
            P_Name = P_Name.Replace(" ", "");
            P_Name = P_Name.Replace("-", "");
            P_Name = P_Name.Replace("+", "");
            P_Name = P_Name.Replace("=", "");
            P_Name = P_Name.Replace("_", "");
            P_Name = P_Name.Replace("�", "");
            P_Name = P_Name.Replace("%", "");
            P_NameOri = P_NameOri.ToUpper();
            P_Name = P_Name.ToUpper();

            int _top = 100;
            string paraContains = "^";
            if (_searchtext.Substring(0, 1) == "%")
            {
                paraContains = "";
            }
            List<SearchOrderEntry> lll = new List<SearchOrderEntry>();
            lll = _dataSearchAll.Where(p =>
                    p.ItemNameSearch.ToUpper().Contains(paraContains + P_Name)
                    || p.Code.ToUpper() == P_Name
                    || p.ItemNameSame.ToUpper().Contains(paraContains + P_Name)
                    || p.CodeSame.ToUpper().Contains(paraContains + P_NameOri)
                    || p.aliasname.ToUpper().Contains(paraContains + P_Name)
                    || p.aliasname.ToUpper().Contains(paraContains + P_NameOri)
                    || String.Join("", p.GenericName.ToUpper().Split(',').Select(p1 => paraContains + p1).ToList()).Contains(paraContains + P_Name)
                    || String.Join("", p.GenericName.ToUpper().Split(',').Select(p1 => paraContains + p1).ToList()).Contains(paraContains + P_NameOri)
                    ).Take(_top).ToList();

            foreach (var item in lll)
            {
                string[] spr = item.aliasname.ToUpper().Split('^');
                if (spr.Where(p => ((p == P_Name) || (p == P_NameOri)) && p != "").ToList().Count > 0)
                    item.orderby = 1;
                else
                    item.orderby = 2;

                if (item.ItemName.ToUpper().Substring(0, 1) == P_NameOri.ToUpper().Substring(0, 1))
                    item.Afarbet = 1;
                else
                    item.Afarbet = 2;
            }
            lll = lll.ToList().OrderByDescending(p => p.grouporder).ThenBy(p => p.CategoryName).ThenBy(p => p.orderby).ThenBy(p => p.Afarbet).ThenBy(p => p.ItemName).ToList();
            return lll;
        }



        public UserLogin getGetUser(string username,string password)
        {
            UserLogin ls = new UserLogin();
            DataTable Dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select UID,CareproviderUID,dbo.fGetCareProviderFullNameNoMiddleByUID(CareproviderUID)Name from Login where loginname=@loginname and Password=@Password and statusflag='A'";
                cmd.Parameters.AddWithValue("@loginname", username);
                cmd.Parameters.AddWithValue("@Password", password);
                Dt.Load(cmd.ExecuteReader());
                if (Dt.Rows.Count > 0)
                {
                    ls = Dt.AsEnumerable().Select(p => new UserLogin()
                    {
                        LoginUID = int.Parse(p["UID"].ToString()),
                        Name = p["Name"].ToString(),
                        Username = username,
                        Password = password,
                        UserUID = int.Parse(p["CareproviderUID"].ToString()),
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

            }
            DisConnect();
            cmd.Dispose();

            return ls;
        }
        public List<RoleDataLocation> getRoleDataLocation(int LoginUID)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable Dt = new DataTable();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "pGetRoleProfileForLogin";
                cmd.Parameters.AddWithValue("@P_LoginUID", LoginUID);
                Dt.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {

            }
            DisConnect();

            var IE = Dt.AsEnumerable().Select(p => new RoleDataLocation()
            {
                RoleUID = int.Parse(p["RoleUID"].ToString()),
                LocationUID = int.Parse(p["LocationUID"].ToString()),
                Description = p["RoleName"].ToString() + " - " + p["LocationName"].ToString(),
            }
            ).ToList();
            return IE;
        }

        public List<ModelAdmission> getPatientAdmission()
        {
            SqlCommand cmd = new SqlCommand();
            DataTable Dt = new DataTable();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"select * from vbdmsadmission where UID in (
select UID from Patientvisit where enddttm is null and statusflag='A' and entypuid=1379
) order by uid desc
select * from ReferenceValue where UID=1379
";
                Dt.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {

            }
            DisConnect();

            var IE = Dt.AsEnumerable().Select(p => new ModelAdmission()
            {
                UID = int.Parse(p["UID"].ToString()),
                PatientUID = int.Parse(p["PatientUID"].ToString()),
                BedName = p["BedName"].ToString(),
                CareProviderName = p["CareProviderName"].ToString(),
                EN = p["EpisodeNumber"].ToString(),
                HN = p["PatientID"].ToString(),
                PatientName = p["PatientName"].ToString(),
                LocationName = p["LocationName"].ToString(),
                Status = p["Status"].ToString(),
                Visitdate = DateTime.Parse(p["StartDttm"].ToString()).ToString("dd/MM/yyyy HH:mm"),
            }
            ).ToList();
            return IE;
        }
        public List<ReferenceValue> getCareProvider()
        {
            return ff.getCareProviderFile();
        }
        public List<ReferenceValue> getLocationFrom()
        {
            return ff.getLocation();
        }


    }
}