﻿using BC.DataProvider.Interface;
using BC.DataProvider.Model;
using BC.DataProvider.Repository;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;

namespace BC.WebAPI.Controllers
{

    public class QuickRegisterController : ApiController
    {
        private PatientRepo repo;
        private ReferenceValue refval;
        public QuickRegisterController()
        {
            repo = new PatientRepo();
            refval = new ReferenceValue();
        }
        //api/quickregister
        [HttpPost]
        public Patient GetPatient([FromBody]Patient _data)
        {
            //var result = new JsonResult(repo.GetPatient(_data.UID), new Newtonsoft.Json.JsonSerializerSettings() {  });
            if (_data.UID >0)
                return repo.GetPatient(_data.UID);
            else if (_data.PASID != null)
                return repo.GetPatientByHN(_data.PASID);
            else
                return new Patient();
        }
        [HttpGet]
        public List<FormConfig> GetFormConfig()
        {
            return repo.GetFormConfig();
        }

        [HttpGet]
        public string Get()
        {
            return "Hello from controller";
        }
        
    }
}