﻿using ModelProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class PatientListController : ApiController
    {
        [HttpPost]
        public List<ModelAdmission> PostGetAdmission([FromBody] ParameterData data)
        {
            dbhelper db = new dbhelper();
            List<ModelAdmission> aa = db.getPatientAdmission();
            return aa;
        }
    }
}
