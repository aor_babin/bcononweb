﻿using ModelProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class LoginController : ApiController
    {
        [HttpPost]
        public UserLogin PostCheckLogin([FromBody] ParameterData data)
        {
            dbhelper db = new dbhelper();
            System.Security.Cryptography.MD5CryptoServiceProvider oProvider = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] barray = System.Text.Encoding.UTF8.GetBytes(data.Password);
            barray = oProvider.ComputeHash(barray);
            string password = Convert.ToBase64String(barray);

            UserLogin aa = db.getGetUser(data.Username, password);
            return aa;
        }
        [HttpPost]
        public List<RoleDataLocation> PostRoleDataLocation([FromBody] ParameterData data)
        {
            dbhelper db = new dbhelper();
            List<RoleDataLocation> aa = db.getRoleDataLocation(data.LoginUID);
            return aa;
        }







        
    }
}
