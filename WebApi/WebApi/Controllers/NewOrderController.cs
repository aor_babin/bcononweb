﻿using ModelProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class NewOrderController : ApiController
    {
        [HttpPost]
        public List<ExistingOrderModel> PostGetExistingOrder([FromBody] ParameterData data)
        {
            dbhelper db = new dbhelper();

            DateTime Start = new DateTime(int.Parse(data.StartDate.Substring(6,4)), int.Parse(data.StartDate.Substring(3, 2)), int.Parse(data.StartDate.Substring(0, 2)));
            DateTime End = new DateTime(int.Parse(data.EndDate.Substring(6, 4)), int.Parse(data.EndDate.Substring(3, 2)), int.Parse(data.EndDate.Substring(0, 2)));
            return db.getBDMSPatientOrderByVisit(data.PatientUID, data.PatientVisitUID, Start, End,data.GroupUID,data.RoleUID).ToList();
        }
        [HttpGet]
        public List<ReferenceValue> GetBDMSGroupCategory()
        {
            dbhelper db = new dbhelper();
            return db.getBDMSGroupCategory();
        }
        [HttpPost]
        public List<ReferenceValue> PostGetVisitAll([FromBody] Patient data)
        {
            dbhelper db = new dbhelper();
            return db.getVisitAll(data.UID).ToList();
        }
        [HttpPost]
        public List<SearchOrderEntry> PostGetAllOrder([FromBody] ParameterData data)
        {
            dbhelper db = new dbhelper();
            return db.getSearchOrderAll(data.PatientVisitUID, data.RoleUID, data.UserUID).ToList();
        }

        [HttpPost]
        public List<SearchOrderEntry> PostGetSearchOrder(SearchOrderParameter _data)
        {
            dbhelper db = new dbhelper();
            return db.SearchInClient(_data.data, _data.SearchText).ToList();
        }



        [HttpGet]
        public List<ReferenceValue> GetCareProvider()
        {
            dbhelper db = new dbhelper();
            return db.getCareProvider();
        }
        [HttpGet]
        public List<ReferenceValue> GetLocationFrom()
        {
            dbhelper db = new dbhelper();
            return db.getLocationFrom();
        }




    }
}
