﻿using ModelProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class PatientController : ApiController
    {
        [HttpPost]
        public ModelAdmission PostPatientAdmission([FromBody] PatientVisit data)
        {
            dbhelper db = new dbhelper();
            return db.getPatient(data);
        }
    }
}
