﻿using BC.DataProvider.Interface;
using BC.DataProvider.Model;
using BC.DataProvider.Repository;
using System.Collections.Generic;
using System.Web.Http;

namespace BC.WebAPI.Controllers
{
  
    public class ValuesController : ApiController
    {
        private ReferenceValue refval;

        public ValuesController()
        {
            refval = new ReferenceValue();
        }

        [HttpPost]
        public List<Reference> GetReferenceValue([FromBody]ReferenceDomain _data)
        {
           
            return refval.GetReferenceValueByDomaincode(_data.DomainCode, EnumReference.Orderby.UID);
        }

        [HttpPost]
        public List<PatientIDConfig> GetPatientIDConfiguration([FromBody]PatientIDConfig _data)
        {
             return refval.GetPatientIDConfig(_data.OrgUID);
        }
    }
}