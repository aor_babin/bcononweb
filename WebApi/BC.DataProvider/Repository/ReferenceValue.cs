﻿using BC.Core;
using BC.DataProvider.Interface;
using BC.DataProvider.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace BC.DataProvider.Repository
{
    public class ReferenceValue 

    {
        private SqlConnection conn;

        public ReferenceValue()
        {
            conn = new SqlConnection(new ConnectionProvider().GetConnectionString());

        }
        void Connect()
        {
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
        }
        void DisConnect()
        {
            if (conn.State == System.Data.ConnectionState.Open)
            {
                conn.Close();
            }
        }

        public List<Reference> GetReferenceValueByDomaincode(string DomainName, EnumReference.Orderby _orderby)
        {
            DataTable Dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            string str = @"select * from referencevalue where domainCode='" + DomainName + @"' order by " + _orderby.ToString() + @" asc";

            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 10000;
                cmd.CommandText = str;

                Dt.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {

            }
            DisConnect();


            return mapReference(Dt);
            
         
        }
        private List<Reference> mapReference(DataTable dt)
        {
            var IE = dt.AsEnumerable().Select(p => new Reference()
            {
                UID = int.Parse(p["UID"].ToString()),
                ValueCode = p["ValueCode"].ToString(),
                Description=p["Description"].ToString()
       
            }).ToList();
            return IE;
        }
        private List<PatientIDConfig> mapPatientIDConfig(DataTable dt)
        {
            var IE = dt.AsEnumerable().Select(p => new PatientIDConfig()
            {
                UID = int.Parse(p["UID"].ToString()),
                PatientClassification=p["PatientClassification"].ToString(),
                SEQName=p["SEQName"].ToString()

            }).ToList();
            return IE;
        }
        public List<PatientIDConfig> GetPatientIDConfig(int ownerOrganisationUID)
        {
            string str = @"select
                                    UID,
                                    PatientClassification,
                                    SEQName
                                    from PatientIDConfiguration
                                    where StatusFlag = 'A' and OwnerOrganisationUID = " + ownerOrganisationUID.ToString() + " Order by UID asc";

            DataTable Dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 10000;
                cmd.CommandText = str;

                Dt.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {

            }
            DisConnect();


            return mapPatientIDConfig(Dt);

        }
    }
}