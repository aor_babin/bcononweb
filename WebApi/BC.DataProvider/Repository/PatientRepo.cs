﻿using BC.Core;
using BC.DataProvider.Interface;
using BC.DataProvider.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace BC.DataProvider.Repository
{
    public class PatientRepo 
    {
        private SqlConnection conn;

        public PatientRepo()
        {
            conn = new SqlConnection(new ConnectionProvider().GetConnectionString());
               
        }
        void Connect()
        {
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
        }
        void DisConnect()
        {
            if (conn.State == System.Data.ConnectionState.Open)
            {
                conn.Close();
            }
        }

        public Patient GetPatientByHN(string hn)
        {
            DataTable Dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 10000;
                cmd.CommandText = "SELECT pt.*,pti.imagecontent FROM Patient pt" +
                    " left join patientimage pti on pt.uid=pti.patientuid and pti.statusflag='A'" +
                    " WHERE pt.pasid='" + hn + "' and  pt.StatusFlag = 'A'";

                Dt.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {

            }
            DisConnect();

            if (Dt != null)
                return mapPatient(Dt);
            else
                return new Patient();

        }
        public Patient GetPatient(int patientUID)
        {
            DataTable Dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 10000;
                cmd.CommandText = "SELECT pt.*,pti.imagecontent FROM Patient pt" +
                    " left join patientimage pti on pt.uid=pti.patientuid and pti.statusflag='A'" +
                    " WHERE pt.uid="+patientUID+" and  pt.StatusFlag = 'A'";
               
                Dt.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {

            }
            DisConnect();

            if (Dt != null)
                return mapPatient(Dt);
            else
                return new Patient();
         
        }

        public List<FormConfig> GetFormConfig()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                Connect();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select Name,IsRequireField from BDMSConfigDemographics";
                dt.Load(cmd.ExecuteReader());
            }
            catch(Exception ex)
            {

            }
            DisConnect();
            if (dt != null)
                return mapConfig(dt);
            else
                return new List<FormConfig>();
        }

        private List<FormConfig> mapConfig(DataTable dt)
        {
            var IE = dt.AsEnumerable().Select(p => new FormConfig()
            {
                Name = p["name"].ToString(),
                IsRequired = p["IsRequireField"].ToString() == "Y" ? true : false

            });
            return IE.ToList();
        }

        private Patient mapPatient(DataTable dt)
        {
            var IE = dt.AsEnumerable().Select(p => new Patient()
            {
                UID = int.Parse(p["UID"].ToString()),
                Forename = p["Forename"].ToString(),
                MiddleName = p["MiddleName"].ToString(),
                Surname = p["Surname"].ToString(),
                SEXXXUID = p["SEXXXUID"].ToString() == "" ? 0 : int.Parse(p["SEXXXUID"].ToString()),
                BLOODUID = p["BLOODUID"].ToString() == "" ? 0 : int.Parse(p["BLOODUID"].ToString()),
                TITLEUID = p["TITLEUID"].ToString() == "" ? 0 : int.Parse(p["TITLEUID"].ToString()),
                DOBComputed = p["DOBComputed"].ToString() == "" ? 0 : int.Parse(p["DOBComputed"].ToString()),
                OCCUPUID = p["OCCUPUID"].ToString() == "" ? 0 : int.Parse(p["OCCUPUID"].ToString()),
                SPOKLUID = p["SPOKLUID"].ToString() == "" ? 0 : int.Parse(p["SPOKLUID"].ToString()),
                MARRYUID = p["MARRYUID"].ToString() == "" ? 0 : int.Parse(p["MARRYUID"].ToString()),
                NATNLUID = p["NATNLUID"].ToString() == "" ? 0 : int.Parse(p["NATNLUID"].ToString()),
                
                DOB = DateTime.Parse(p["BirthDttm"].ToString()).ToString("dd/MM/yyyy"),
                PASID = p["PASID"].ToString(),
                IsVIP = p["IsVIP"].ToString(),
                VIPTPUID = p["VIPTPUID"].ToString() == "" ? 0 : int.Parse(p["VIPTPUID"].ToString()),
                SEQName = p["SEQName"].ToString(),
                ETHGPUID = p["ETHGPUID"].ToString() == "" ? 0 : int.Parse(p["ETHGPUID"].ToString()),
                RELGNUID = p["RELGNUID"].ToString() == "" ? 0 : int.Parse(p["RELGNUID"].ToString()),
                IsAnonymous = p["IsAnonymous"].ToString(),
                RNEWSUID = p["RNEWSUID"].ToString() == "" ? 0 : int.Parse(p["RNEWSUID"].ToString()),
                PatientImage = p["ImageContent"].ToString() == "" ? null : ConvertToBase64((byte[])p["ImageContent"])
            }).ToList();

             return IE[0];
        }
            private List<Patient> mapPatientList(DataTable dt)
        {
            var IE = dt.AsEnumerable().Select(p => new Patient()
            {
                UID = int.Parse(p["UID"].ToString()),
                Forename = p["Forename"].ToString(),
                MiddleName = p["MiddleName"].ToString(),
                Surname = p["Surname"].ToString(),
                SEXXXUID = p["SEXXXUID"].ToString() == "" ? 0 : int.Parse(p["SEXXXUID"].ToString()),
                BLOODUID = p["BLOODUID"].ToString() == "" ? 0 : int.Parse(p["BLOODUID"].ToString()),
                TITLEUID = p["TITLEUID"].ToString() == "" ? 0 : int.Parse(p["TITLEUID"].ToString()),
                DOBComputed = p["DOBComputed"].ToString() == "" ? 0 : int.Parse(p["DOBComputed"].ToString()),
                OCCUPUID = p["OCCUPUID"].ToString() == "" ? 0 : int.Parse(p["OCCUPUID"].ToString()),
                SPOKLUID = p["SPOKLUID"].ToString() == "" ? 0 : int.Parse(p["SPOKLUID"].ToString()),
                MARRYUID = p["MARRYUID"].ToString() == "" ? 0 : int.Parse(p["MARRYUID"].ToString()),
                NATNLUID = p["NATNLUID"].ToString() == "" ? 0 : int.Parse(p["NATNLUID"].ToString()),
                
                DOB = DateTime.Parse(p["BirthDttm"].ToString()).ToString("dd/MM/yyyy"),
                PASID = p["PASID"].ToString(),
                IsVIP = p["IsVIP"].ToString(),
                VIPTPUID = p["VIPTPUID"].ToString() == "" ? 0 : int.Parse(p["VIPTPUID"].ToString()),
                SEQName = p["SEQName"].ToString(),
                ETHGPUID = p["ETHGPUID"].ToString() == "" ? 0 : int.Parse(p["ETHGPUID"].ToString()),
                RELGNUID = p["RELGNUID"].ToString() == "" ? 0 : int.Parse(p["RELGNUID"].ToString()),
                IsAnonymous = p["IsAnonymous"].ToString(),
                RNEWSUID = p["RNEWSUID"].ToString() == "" ? 0 : int.Parse(p["RNEWSUID"].ToString())
            }).ToList();
            return IE;
        }
        //public Patient GetPatient(string HN)
        //{
            
        //    DataTable Dt = new DataTable();
        //    SqlCommand cmd = new SqlCommand();
        //    try
        //    {
        //        Connect();
        //        cmd.Connection = conn;
        //        cmd.CommandType = CommandType.Text;
        //        cmd.CommandTimeout = 10000;
        //        cmd.CommandText = "SELECT * FROM Patient WHERE StatusFlag = 'A' AND PASID ='"+HN+"'";

        //        Dt.Load(cmd.ExecuteReader());
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    DisConnect();

        //    if (Dt != null)
        //        return mapPatient(Dt);
        //    else
        //        return new Patient();
           
        //}
    
       private string ConvertToBase64(byte[] img)
        {
            return System.Convert.ToBase64String(img);
        }
        private byte[] ConvertToByte(string img)
        {
            return System.Convert.FromBase64String(img);
        }
    }
}