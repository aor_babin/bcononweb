﻿using BC.DataProvider.Model;
using System.Collections.Generic;

namespace BC.DataProvider.Interface
{
    public interface IPatientRepo
    {
        Patient GetPatient(int patientUID);

        Patient GetPatient(string HN);

        IList<Patient> FindPatient(string forename, string surname);

        IList<Patient> GetPatients();
    }
}