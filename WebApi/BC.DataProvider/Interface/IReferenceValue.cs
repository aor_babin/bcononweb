﻿using BC.DataProvider.Model;
using System.Collections.Generic;

namespace BC.DataProvider.Interface
{
    public interface IReferenceValue
    {
        IList<Reference> GetReferenceValueByDomaincode(string DomainName, EnumReference.Orderby _orderby);
        IList<PatientIDConfig> GetPatientIDConfig(int ownerOrganisationUID);
    }
}