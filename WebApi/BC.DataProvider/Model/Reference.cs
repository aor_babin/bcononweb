﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BC.DataProvider.Model
{
    public class Reference
    {
        public int UID { get; set; }
        public string ValueCode { get; set; }
        public string Description { get; set; }
        
    }
    public class ReferenceDomain
    {
        public string DomainCode { get; set; }
    }
    public class EnumReference
    {
        public enum Orderby { UID, DisplayOrder, Description };
    }
    public class PatientIDConfig
    {
        public int UID { get; set; }
        public string PatientClassification { get; set; }
        public string SEQName { get; set; }
        public int OrgUID { get; set; }
    }
    
}
