﻿using System;

namespace BC.DataProvider.Model
{
    public class Patient
    {
        public int UID { get; set; }
        public string Forename { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public int SEXXXUID { get; set; }
        public int BLOODUID { get; set; }
        public int TITLEUID { get; set; }
        public int DOBComputed { get; set; }
        public int OCCUPUID  {get;set;}
        public int SPOKLUID { get; set; }
        public int MARRYUID { get; set; }
        public int NATNLUID { get; set; }
        public DateTime BirthDttm { get; set; }
        public string PASID { get; set; }
        public string IsVIP  { get; set; }
        public int VIPTPUID { get; set; }
        public string SEQName { get; set; }
        public int ETHGPUID { get; set; }
        public int RELGNUID { get; set; }
        public string IsAnonymous { get; set; }
        public int RNEWSUID { get; set; }
        public string DOB { get; set; }
        public string PatientImage { get; set; } 
    }

    public class FormConfig
    {
       public string Name { get; set; }
        public bool IsRequired { get; set; }
    }
}