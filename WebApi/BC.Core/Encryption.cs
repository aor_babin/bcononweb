﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace BC.Core
{
    public class Encryption : IEncryption
    {
        #region Private Properties

        private byte[] _key { get; set; }
        private byte[] _vector { get; set; }

        #endregion Private Properties

        #region Constructor

        public Encryption()
            : this("123@idea")
        {
        }

        public Encryption(string phase)
        {
            GenerateKey(phase);
        }

        #endregion Constructor

        #region Public Methods

        public string Decrypt(string encryptText, string phase)
        {
            var array = Convert.FromBase64String(encryptText);
            var rijndaelManaged = Aes.Create();
            var transform = rijndaelManaged.CreateDecryptor(_key, _vector);
            var memoryStream = new MemoryStream(array);
            var cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read);
            var array2 = new byte[array.Length];
            var count = cryptoStream.Read(array2, 0, array2.Length);
            return new ASCIIEncoding().GetString(array2, 0, count);
        }

        public string Encrypt(string plainText, string phase)
        {
            var bytes = new ASCIIEncoding().GetBytes(plainText);
            var rijndaelManaged = Aes.Create();
            var transform = rijndaelManaged.CreateEncryptor(_key, _vector);
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(bytes, 0, bytes.Length);
            cryptoStream.FlushFinalBlock();
            return Convert.ToBase64String(memoryStream.ToArray());
        }

        #endregion Public Methods

        #region Private Methods

        private void GenerateKey(string phase)
        {
            var sHA384Managed = SHA384.Create();
            var sourceArray = sHA384Managed.ComputeHash(new ASCIIEncoding().GetBytes(phase));
            _key = new byte[32];
            _vector = new byte[16];
            Array.Copy(sourceArray, 0, _key, 0, 32);
            Array.Copy(sourceArray, 32, _vector, 0, 16);
        }

        #endregion Private Methods
    }
}