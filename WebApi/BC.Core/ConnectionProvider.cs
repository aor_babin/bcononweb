﻿using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BC.Core
{
    public class ConnectionProvider : IConnectionProvider
    {
        #region Properties

        private static IDbConnection connection;
        private IEncryption encryption;

        #endregion Properties

        #region Constructor

        public ConnectionProvider()
        {
            encryption = new Encryption();
        }

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// Get an existing database connection or Create new one.
        /// </summary>
        /// <returns>SqlConnection object</returns>
        public IDbConnection GetConnection()
        {
            return GetConnection("defaultConnection");
        }

        /// <summary>
        /// Get an existing database connection or Create new one.
        /// </summary>
        /// <param name="key">Connection key</param>
        /// <returns>SqlConnection object</returns>
        public IDbConnection GetConnection(string key)
        {
            if (connection == null)
            {
                try
                {
                    var connectionString = GetConnectionString(key);
                    if (string.IsNullOrEmpty(connectionString))
                    {
                        throw new Exception("Invalid connection string.");
                    }
                    else
                    {
                        connection = new SqlConnection(connectionString);
                        connection.Open();
                        return connection;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
                

                if (connection.State != ConnectionState.Open)
                {  try
                    {
                        connection.Open();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return connection;
            }
        }
        
        #endregion Public Methods

        #region Private Methods

        public string GetConnectionString()
        {
            return GetConnectionString("defaultConnection");
            //return "Data Source=10.103.13.13;Initial Catalog=BNH_testpatch(dev);Persist Security Info=True;User ID=gen;Password=gen.641;";
        }

        private string GetConnectionString(string key)
        {
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            string value = "";
            if (key == "defaultConnection")
            {
                value = configuration.GetSection(key).Value;
            }
            else
            {
                value = configuration.GetSection($"ConnectionString:{key}").Value;
            }

            if (string.IsNullOrEmpty(value))
            {
                throw new Exception("Invalid Key configuration");
            }
            else
            {
                value = encryption.Decrypt(value, "123@idea");
            }

            return value;
        }

        #endregion Private Methods
    }
}