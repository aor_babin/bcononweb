﻿namespace BC.Core
{
    public interface IEncryption
    {
        string Encrypt(string plainText, string phase);

        string Decrypt(string cryptText, string phase);
    }
}