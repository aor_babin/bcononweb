﻿using System.Data;

namespace BC.Core
{
    public interface IConnectionProvider
    {
        IDbConnection GetConnection();

        IDbConnection GetConnection(string key);
    }
}